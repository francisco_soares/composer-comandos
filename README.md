# Composer comandos

_Aprimorando meus estudo sobre o composer._
<br><br>

## **Instalando o composer**
----------------------------<br>
Vá até o [getcomposer.org](https://getcomposer.org/) e faça o download clicando em **Getting Started** > **Installation - Windows** > [Composer-Setup.exe](https://getcomposer.org/Composer-Setup.exe)
> Lembrando que deve instalar o mesmo globalmente.
<br><br>

## **Baixando uma dependência**
----------------------------<br>
No terminal - **Exemplo: PHPMailer**<br>
> **composer require phpmailer/phpmailer**<br>
_Lembrando que o arquivo **composer.json** deve estar criado._<br><br>

## **Autoload**
----------------------------<br>
Para usar o composer, você precisa instânciar a classe Autoload do Composer<br>
Para isso, use o comando abaixo na sua index.php<br>
> **require_once "vendor/autoload.php"**
<br><br>

## **Packagist**
O Packagist é um repositório onde vamos baixar todas as dependências que precisamos para nosso projeto.<br>
Para acessar o [Packagist](https://packagist.org/).
<br><br>

## **Instalando o Composer via Terminal**
----------------------------<br>
> **composer init**<br>
Faça as escolhas e depois vai para a instalação.

> **composer install**
<br>

Para atualizar o composer
> **composer update**
<br><br>

## **Autoload - Prefixos**
----------------------------<br>
Para definir os prefixos, você precisa fazendo do arquivo **composer.json** a inserção do seguinte código:<br>
> **"autoload": {"psr-4": { "App\\": "App" }}**<br>
Lembrando que os prefixos deve ser de acordo com a estruturar do seu projeto, então o **App** pode ser **Config** ou qualquer outro nome.<br>
Você também pode inserir vários prefixos.<br>

Após ter escolhido os preximos, use o seguinte comando
> **composer dump-autoload -o**<br><br>

Outro exemplo de prefixo:<br>
> **"autoload": {"psr-4": { "Config\\": ["services", "jobs"] }}**<br>
Isso indica que o composer vai procurar dentro das pastas **services** e **jobs**, pois ambas estão usando o prefixo **Config**.<br><br>

**Carregando uma função**
> **"autoload": {"files": ["functions/helpers", "function/teste.php"] }**<br>
Assim você está inserido um arquivo de função ao Autoload do Composer.<br>
Sempre lembre de usar o comando:<br>
> **composer dump-autoload -o**<br><br>

## **Comandos básicos**


```
composer update
```
Caso deseje inserir ou remover pacotes ou alterar o autoload devemos sempre atualizar nosso projeto, e com esse comando tudo o que listei é feito de forma automática sem dores de cabeça<br><br>

```
composer self-update
```
Como todo sistema o composer recebe constantemente atualizações, e  para atualiza-lo utilizamos o self-update.<br><br>

```
composer dump-autoload ou dumpautoload
```
Toda vez que houver necessidade de atualizar o autoloader do nosso sistema, podemos utilizar uma dessas duas versões do comando para tal atividade, ambas no final das contas obtém o mesmo resultado.<br><br>


## **Estrutura básica**

```
{
    "name": "tiesco-assis/composer-comandos",
    "description": "Entendendo a sintax do composer",
    "authors": [
        {
            "name": "Francisco Soares",
            "email": "francisco.sts@hotmail.com",
            "role": "Developer"
        }
    ],
    "minimum-stability": "stable",
    "require": {
        "php": ">= 7.0",
        "phpmailer/phpmailer": "^6.5"
    },
    "autoload": {
        "psr-4": {
            "Config\\": "config/"
        }
    }
}
```
**name**: É o seu nome lá no git e projeto <br>
**description**: Descrição do projeto <br>

**authors**: Aqui vai entrar informações do autor do projeto, aqui vou lista três: **name**, **email** e **role** <br>
- **name**: Seu nome <br>
- **email**: Seu e-mail <br>
- **role**: Sua função ou cargo <br>

**minimum-stability**: Isso define o comportamento padrão para filtrar pacotes por estabilidade<br>

**require**:  Pacotes exigidos por este pacote. Exemplo de pacote exigidos<br>
- **php >= 7.0** <br>

**autoload**:  Carregamento automático de diretório<br>
- **psr-4**: Padrão usado pelo PHP para adotar um padrão de codificação. <br>
    - **"Config\\": "config/"**: É a pasta que será mapeada pelo autoload seguinte a PRS-4 <br>
